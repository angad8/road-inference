import os
import glob
from os import environ
import sys
import warnings
from functools import wraps
from qgis.core import *
import glob

# Ground truth shp
src_path1 = "/home/skylark/benchamrk/vegetation/kannur/Vegetation_reproject.geojson"
# Cleaned simplified - after post processing
src_path2 = "/home/skylark/benchamrk/vegetation/kannur/Features_partial.geojson"
# Regularised buildings - arcgis
#src_path3 = "/home/skylark/Kritz/veg_deploy_v1/data/predictions/dd-ml-3/ortho_reprojected_3band_qubvel_output.shp"

environ["QT_QPA_PLATFORM"] = "offscreen"


def ignore_warnings(f):
    @wraps(f)
    def inner(*args, **kwargs):
        with warnings.catch_warnings(record=True):
            warnings.simplefilter("ignore")
            response = f(*args, **kwargs)
        return response

    return inner

@ignore_warnings
def setup_qgis_env():
    """
    Set paths and start QGIS application
    :return: QGIS application instance
    """
    sys.path.append("/usr/lib/qgis")
    sys.path.append("/usr/bin/python3")
    sys.path.append("/usr/share/qgis/python/plugins")
    #sys.path.append("/home/skylark/anaconda3/bin/python3")
    from qgis.core import QgsApplication

    app = QgsApplication([], False)
    return app

@ignore_warnings
def setup_proc_toolbox(app):
    """
    Load processing toolbox into QGIS application
    :param app: QGIS application instance
    :return: updated QGIS applicaiton instance,
             processing toolbox instance
    """
    import processing
    from processing.core.Processing import Processing
    from qgis.analysis import QgsNativeAlgorithms

    Processing.initialize()
    app.processingRegistry().addProvider(QgsNativeAlgorithms())

    # for alg in app.processingRegistry().algorithms():
    #     print(alg.id(), '->', alg.displayName())

    return (app, processing)


def create_project(project_loc):
    """
    Create a QGIS project
    :param project_loc: path to create QGIS project
    :return: QGIS project instance
    """
    from qgis.core import QgsProject

    project = QgsProject.instance()
    project.setFileName(project_loc)
    project.write()
    return project


def add_raster_layer(project, raster, layer_name):
    """
    Add required raster layer to the QGIS project
    :param project: QGIS project instance
    :param raster: path to raster file
    :return: raster layer instance
    """
    from qgis.core import QgsRasterLayer

    rlayer = QgsRasterLayer(raster, layer_name, "gdal")
    if not rlayer.isValid():
        raise Exception("Invalid raster")

    project.addMapLayer(rlayer)
    return rlayer


def add_vector_layer(project, vector, layer_name):
    """
    Add required vector layer to the QGIS project
    :param project: QGIS project instance
    :param vector: path to vector file
    :return: vector layer instance
    """
    from qgis.core import QgsVectorLayer

    vlayer = QgsVectorLayer(vector, layer_name, "ogr")
    if not vlayer.isValid():
        raise Exception("Invalid vector")

    project.addMapLayer(vlayer)
    return vlayer

def intersect(processing, inp_shapef1, inp_shapef2, op_shapef):
    params = {
        "INPUT": inp_shapef1,
        "OVERLAY": inp_shapef2,
        "OUTPUT": op_shapef
    }
    processing.run("qgis:intersection", params)

def union(processing, inp_shapef1, inp_shapef2, op_shapef):
    params = {
        "INPUT": inp_shapef1,
        "OVERLAY": inp_shapef2,
        "OUTPUT": op_shapef
    }
    processing.run("qgis:union", params)

def main(shp_files):
    files_del = []
    project_loc = "/home/skylark/dummy.qgs"
    files_del.append(project_loc)
    app = setup_qgis_env()
    app.initQgis()
    app, processing = setup_proc_toolbox(app)

    project = create_project(project_loc=project_loc)

    d_path_inter = "/home/skylark/Kritz/veg_deploy_v1/data/inter.shp"
    d_path_union = "/home/skylark/Kritz/veg_deploy_v1/data/union.shp"

    vec1 = QgsProcessingFeatureSourceDefinition(shp_files[0], selectedFeaturesOnly=False, featureLimit=-1, flags=QgsProcessingFeatureSourceDefinition.FlagOverrideDefaultGeometryCheck, geometryCheck=QgsFeatureRequest.GeometrySkipInvalid)
    vec2 = QgsProcessingFeatureSourceDefinition(shp_files[1], selectedFeaturesOnly=False, featureLimit=-1, flags=QgsProcessingFeatureSourceDefinition.FlagOverrideDefaultGeometryCheck, geometryCheck=QgsFeatureRequest.GeometrySkipInvalid)

    intersect(processing, inp_shapef1 = vec1, inp_shapef2 = vec2, op_shapef = d_path_inter)
    union(processing, inp_shapef1 = vec1, inp_shapef2 = vec2, op_shapef = d_path_union)

    _ = glob.glob(d_path_inter.split(".")[0]+"*")
    for i in _:
        files_del.append(i)    
    _ = glob.glob(d_path_union.split(".")[0]+"*")
    for i in _:
        files_del.append(i)    
    
    layer = QgsVectorLayer(d_path_inter, 'layer' , 'ogr')
    area_inter = 0
    for feat in layer.getFeatures():
        area_inter += feat.geometry().area()
    
    layer = QgsVectorLayer(d_path_union, 'layer' , 'ogr')
    area_union = 0
    for feat in layer.getFeatures():
        area_union += feat.geometry().area()
    
    print("IOU: ",area_inter/area_union)
    #app.exitQgis()

    for i in files_del:
            os.remove(i)

print("GT & inf1 IOU:")
main([src_path1, src_path2])
#print("GT & inf2 IOU:")
#main([src_path1, src_path3])
