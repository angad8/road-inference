import argparse
import os
import rasterio 
from os import environ,system
import sys
import warnings
from rasterio import features
from shapely.geometry import shape, Polygon 
from functools import wraps
import geopandas as gpd

parser = argparse.ArgumentParser()
parser.add_argument('--url', type=str, help="s3 URL of ortho whose inference is to be taken")
parser.add_argument('--ortho_name', type=str, help="Folder name")

args = parser.parse_args()
environ["QT_QPA_PLATFORM"] = "offscreen"

d_path = "/home/skylark/Angad/road_deploy_v1/data/predictions/"
if not os.path.exists(d_path):
    os.mkdir(d_path)

file_path = d_path + args.ortho_name
if not os.path.exists(file_path):
    os.mkdir(file_path)


def pred(raster_path, file_path):
    """
    Reads Orthomosaic, convert's it to 0.1m pixel size, check band and take prediction

    Args:
        raster_path: tif file on which prediction has to be taken
    Returns:
        results: stores a shapefile with the same name of the input file name
    """
 
    #raster_path.split(".tif")[0].split(".com")[1].split("/")[4]
    tmp_path = os.path.join(file_path, raster_path.split(".tif")[0].split(".com")[1].split("/")[4]+".tif")
    if not os.path.exists(tmp_path):
        cmd = 'wget -O {1} "{0}"'.format(raster_path,tmp_path)
        os.system(cmd)
    
    files_created = []
    
    reproj_path = tmp_path.split(".tif")[0]+"_reprojected.tif"
    if not os.path.exists(reproj_path):
        #os.remove(reproj_path)
        cmd = "gdalwarp -tr 0.1 0.1 -r near -nosrcalpha -of GTiff {0} {1}".format(tmp_path, reproj_path)
        os.system(cmd)
        files_created.append(reproj_path)

    r = rasterio.open(reproj_path)
    count = r.count
    print(count)
    if count==4:
        pred_file = tmp_path.split(".tif")[0]+"_reprojected_3band.tif"
        if not os.path.exists(pred_file):
            cmd = "gdal_translate -b 1 -b 2 -b 3 {0} {1}".format(reproj_path, pred_file)
            os.system(cmd)
            files_created.append(pred_file)

    elif count==3:
        pred_file = reproj_path
    else:
        print("Required Input is 3 band raster")
        return files_created
   

    op_pth = pred_file.split(".tif")[0]+"_output.tif"
    if not os.path.exists(op_pth):
        cmd = "python3 -m src.predict_tif  --src_path {0} --dst_path {1} --batch_size 16 --gpu 0 --tta".format(pred_file,op_pth)
        os.system(cmd)
        files_created.append(op_pth)
    return files_created


def vectorize_inferences(raster, pixel_value, area_threshold):
    raster_tile = rasterio.open(raster)
    raster_data_band_1 = raster_tile.read(1)
    if pixel_value not in raster_data_band_1:
        print("No valid inference found!")
        exit()

    shapes = list(features.shapes(raster_data_band_1, transform=raster_tile.transform))

    polygons = [shape(geom) for geom, value in shapes
                if (value == pixel_value and shape(geom).area > area_threshold)]
    simple = [poly.simplify(0.1) for poly in polygons]
    gdf = gpd.GeoDataFrame(geometry=simple)
    gdf.crs = raster_tile.crs
    
    file_name_shp = raster.split(".")[0] +'.shp'
    gdf.to_file(driver="ESRI Shapefile", filename=file_name_shp)
    
    gdf = gdf.to_crs({'init': 'epsg:4326'})
    file_name_geojson = raster.split(".")[0] + '.geojson'
    gdf.to_file(driver="GeoJSON", filename=file_name_geojson)
    return file_name_shp


def vectorize_no_processing(raster, pixel_value):
    raster_tile = rasterio.open(raster)
    raster_data_band_1 = raster_tile.read(1)
    if pixel_value not in raster_data_band_1:
        print("No valid inference found!")
        exit()
    shapes = list(features.shapes(raster_data_band_1, transform=raster_tile.transform))
    polygons = [shape(geom) for geom, value in shapes]
    gdf = gpd.GeoDataFrame(geometry=polygons)
    gdf.crs = raster_tile.crs
    
    file_name_shp = raster.split(".")[0] +'.shp'
    gdf.to_file(driver="ESRI Shapefile", filename=file_name_shp)
    
    gdf = gdf.to_crs({'init': 'epsg:4326'})
    file_name_geojson = raster.split(".")[0] + '.geojson'
    gdf.to_file(driver="GeoJSON", filename=file_name_geojson)
    return file_name_shp


def ignore_warnings(f):
    @wraps(f)
    def inner(*args, **kwargs):
        with warnings.catch_warnings(record=True):
            warnings.simplefilter("ignore")
            response = f(*args, **kwargs)
        return response

    return inner


@ignore_warnings
def setup_qgis_env():
    """
    Set paths and start QGIS application
    :return: QGIS application instance
    """
    sys.path.append("/usr/lib/qgis")
    sys.path.append("/usr/bin/python3")
    sys.path.append("/usr/share/qgis/python/plugins")
    from qgis.core import QgsApplication

    app = QgsApplication([], False)
    return app


@ignore_warnings
def setup_proc_toolbox(app):
    """
    Load processing toolbox into QGIS application
    :param app: QGIS application instance
    :return: updated QGIS applicaiton instance,
             processing toolbox instance
    """
    import processing
    from processing.core.Processing import Processing
    from qgis.analysis import QgsNativeAlgorithms

    Processing.initialize()
    app.processingRegistry().addProvider(QgsNativeAlgorithms())

    # for alg in app.processingRegistry().algorithms():
    #     print(alg.id(), '->', alg.displayName())

    return (app, processing)


def create_project(project_loc):
    """
    Create a QGIS project
    :param project_loc: path to create QGIS project
    :return: QGIS project instance
    """
    from qgis.core import QgsProject

    project = QgsProject.instance()
    project.setFileName(project_loc)
    project.write()
    return project


def add_raster_layer(project, raster, layer_name):
    """
    Add required raster layer to the QGIS project
    :param project: QGIS project instance
    :param raster: path to raster file
    :return: raster layer instance
    """
    from qgis.core import QgsRasterLayer

    rlayer = QgsRasterLayer(raster, layer_name, "gdal")
    if not rlayer.isValid():
        raise Exception("Invalid raster")

    project.addMapLayer(rlayer)
    return rlayer


def add_vector_layer(project, vector, layer_name):
    """
    Add required vector layer to the QGIS project
    :param project: QGIS project instance
    :param vector: path to vector file
    :return: vector layer instance
    """
    from qgis.core import QgsVectorLayer

    vlayer = QgsVectorLayer(vector, layer_name, "ogr")
    if not vlayer.isValid():
        raise Exception("Invalid vector")

    project.addMapLayer(vlayer)
    return vlayer

def delete_holes(processing, inp_shapef, op_shapef):
    params = {
        "INPUT": inp_shapef,
        "OUTPUT": op_shapef,
        "MIN_AREA": 0
    }
    processing.run("native:deleteholes", params)


def main(url, file_path):

    files = pred(url, file_path)
    shp_file_no_process = vectorize_no_processing(files[-1], pixel_value=1)

    print("Reached shp_file_no_process")

    #files.append(shp_file_no_process)

    shp_file = vectorize_inferences(files[-1], pixel_value=1, area_threshold=100)
    files.append(shp_file)

    shp_op_file = shp_file.split(".shp")[0]+"_cleaned.shp"
    project_loc = "/home/ubuntu/dummy.qgs"

    app = setup_qgis_env()
    app.initQgis()
    app, processing = setup_proc_toolbox(app)

    project = create_project(project_loc=project_loc)
    print("Inferences Calculated")
    #delete_holes(processing, inp_shapef = shp_file, op_shapef = shp_op_file)
    files.append(project_loc)
    app.exitQgis()

    #for i in files:
    #        os.remove(i)

main(args.url, file_path)
