import os
import ttach
import torch
import argparse
from .training.runner import GPUNormRunner
from .training.predictor import TorchTifPredictor

model_path = "/home/skylark/Angad/road_deploy_v1/models/unet_effb3_aug_0.9058.pth"
model = torch.load(model_path)

def main(args, model):
    # set GPUS
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("Available devices:", device)

    # add test time augmentations (flipping and rotating input image)
    if args.tta:
        model = ttach.SegmentationTTAWrapper(model, ttach.aliases.d4_transform(), merge_mode='mean')
    
    # create Multi GPU model if number of GPUs is more than one
    n_gpus = len(args.gpu.split(","))
    if n_gpus > 1:
        gpus = list(range(n_gpus))
        model = torch.nn.DataParallel(model, gpus)
    
    print("Done loading...")
    model.to(device)

    # --------------------------------------------------
    # start evaluation
    # --------------------------------------------------
    runner = GPUNormRunner(model, model_device=device)
    model.eval()

    # predict big tif files
    predictor = TorchTifPredictor(
        runner=runner, sample_size=1024, cut_edge=256,
        batch_size=args.batch_size,
        count=1, NBITS=1, compress=None, driver="GTiff",
        blockxsize=1280, blockysize=1280, tiled=True, BIGTIFF='IF_NEEDED',
    )

    
    print(f"Inference for {args.src_path}")
    predictor(args.src_path, args.dst_path)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--src_path', type=str, required=True,
        help="path to RGB tif file, it is recommended to reproject file" + \
        " to UTM zone and resample to 0.1 m/pixel resolution"
    )
    parser.add_argument(
        '--dst_path', type=str, required=True,
        help="Path where preiction will be saved",
    )
    parser.add_argument('--batch_size', type=int, default=8, help="Number of images in batch")
    parser.add_argument('--gpu', type=str, default='0', help="GPUs to use, e.g. --gpu 0,1,2")
    parser.add_argument('--tta', action='store_true', help="Flag to enable test time augmentation")
    args = parser.parse_args()

    main(args, model)
    os._exit(0)