import argparse
import os
import rasterio 
from os import environ
from rasterio import features
from shapely.geometry import shape
import geopandas as gpd
from osgeo import gdal
import utm

parser = argparse.ArgumentParser()
parser.add_argument('--ortho_name', type=str, help="Folder name", default="Satellite_data")
args = parser.parse_args()
environ["QT_QPA_PLATFORM"] = "offscreen"

tmp_path = "/home/skylark/Downloads/sakhigopal_cog.tif"
d_path = "/home/skylark/Kritz/veg_deploy_v1/data/predictions/"

if not os.path.exists(d_path):
    os.mkdir(d_path)

file_path = d_path + args.ortho_name
if not os.path.exists(file_path):
    os.mkdir(file_path)


def pred(file_path, tmp_path):
    files_created = []
    reproj_path = os.path.join(file_path, "ortho_reprojected.tif")
    #tmp_path.split(".tif")[0]+"_reprojected.tif"
    if not os.path.exists(reproj_path):
        #os.remove(reproj_path)

        tif = gdal.Open(tmp_path)
        tif_crs = rasterio.open(tmp_path)

        reproj_raster = reproj_path

        if tif_crs.crs.to_epsg() == 4326:
            lon, lat = tif.GetGeoTransform()[0], tif.GetGeoTransform()[3]
            utm_zone = utm.from_latlon(lat, lon)[2]
            cmd = "gdalwarp -t_srs EPSG:326{0} -tr 0.1 0.1 -r near -nosrcalpha -of GTiff {1} {2}" \
            .format(utm_zone, tmp_path, reproj_raster)
            os.system(cmd)
        else:
            cmd = f'gdalwarp -tr 0.1 0.1 -r near -nosrcalpha -of GTiff {tmp_path} {reproj_raster}'
            os.system(cmd)

        files_created.append(reproj_path)

    r = rasterio.open(reproj_path)
    count = r.count

    print(count)
    if count==4:
        pred_file = os.path.join(file_path, "ortho_reprojected_3band.tif")
        #tmp_path.split(".tif")[0]+"_reprojected_3band.tif"
        if not os.path.exists(pred_file):
            cmd = "gdal_translate -b 1 -b 2 -b 3 {0} {1}".format(reproj_path, pred_file)
            os.system(cmd)
            files_created.append(pred_file)

    elif count==3:
        pred_file = reproj_path
    else:
        print("Required Input is 3 band raster")
        return files_created
    op_pth = pred_file.split(".tif")[0]+"_qubvel_output.tif"
    if not os.path.exists(op_pth):
        cmd = "python3 -m src.predict_tif --src_path {0} --dst_path {1} --batch_size 2 --gpu 0 --tta".format(pred_file,op_pth)
        os.system(cmd)
        files_created.append(op_pth)
    return files_created


def vectorize_inferences(raster, pixel_value):
    raster_tile = rasterio.open(raster)
    raster_data_band_1 = raster_tile.read(1)
    if pixel_value not in raster_data_band_1:
        print("No valid inference found!")
        exit()

    shapes = list(features.shapes(raster_data_band_1, transform=raster_tile.transform))

    polygons = [shape(geom) for geom, value in shapes
                if (value == pixel_value and shape(geom).area > 10)]
    simple = [poly.simplify(0.1) for poly in polygons]
    gdf = gpd.GeoDataFrame(geometry=simple)
    gdf.crs = raster_tile.crs
    
    file_name_shp = raster.split(".")[0] +'.shp'
    gdf.to_file(driver="ESRI Shapefile", filename=file_name_shp)
    
    gdf = gdf.to_crs({'init': 'epsg:4326'})
    file_name_geojson = raster.split(".")[0] + '.geojson'
    gdf.to_file(driver="GeoJSON", filename=file_name_geojson)
    return file_name_shp


def main():
    files = pred(file_path, tmp_path)
    vectorize_inferences(files[-1], pixel_value=1)
    #for i in files:
    #    os.remove(i)

main()
