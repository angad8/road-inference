import rasterio
from rasterio.mask import mask
import geopandas as gpd
import os, sys
import numpy as np
from tqdm import tqdm
from PIL import Image, ImageFilter
from skimage.transform import resize
from rasterio import features
from rasterio.windows import Window
from shapely.geometry import shape, Polygon 
import math
import os
import cv2

'''
ip_path_imgs = "/home/skylark/Downloads/Open_AI_Cities/OCAI/Images/"
ip_path_lbls = "/home/skylark/Downloads/Open_AI_Cities/OCAI/Labels/"
op_path_imgs = "/home/skylark/AI/OpenCitiesAI/open_cities_ai_tiles/Building/Images/"
op_path_lbls = "/home/skylark/AI/OpenCitiesAI/open_cities_ai_tiles/Building/Labels/"
no_build_img = "/home/skylark/AI/OpenCitiesAI/open_cities_ai_tiles/No_Building/Images/"
no_build_lbl = "/home/skylark/AI/OpenCitiesAI/open_cities_ai_tiles/No_Building/Labels/"
'''

ip_path_imgs = "../../../../../media/skylark/Data/Road_data/Road_Titus_Anot/ortho_clipped/"
ip_path_lbls = "../../../../../media/skylark/Data/Road_data/Road_Titus_Anot/rasterized_annotation/"
op_path_imgs = "../../../../../media/skylark/Data/Road_data/Road_Titus_Anot/ortho_tiles/"
op_path_lbls = "../../../../../media/skylark/Data/Road_data/Road_Titus_Anot/annot_tiles/"


ip_imgs = sorted([(ip_path_imgs + i) for i in os.listdir(ip_path_imgs)])
ip_lbls = sorted([(ip_path_lbls + i) for i in os.listdir(ip_path_lbls)])

assert len(ip_imgs)==len(ip_lbls)
count= 0
for img_tif, lbl_tif in zip(ip_imgs,ip_lbls):
    op_name = (os.path.basename(img_tif)).split(".")[0]
    print(count)
    count+=1
    with rasterio.open(img_tif, 'r') as src:
        label = rasterio.open(lbl_tif)
        print("img:", src.width, src.height)
        print("label:", label.width, label.height)
        sx = src.shape[1]
        sy = src.shape[0]
        size = 1024
        out_profile = src.profile.copy()
        c=-1
        #dummy = np.zeros((1,src.shape[0],src.shape[1]),np.uint8)
        with tqdm(total=math.ceil(sx/size) * math.ceil(sy/size)) as pbar:
            for i in range(0,math.ceil(sx/size)):
                for j in range(0, math.ceil(sy/size)):
                    col_off = i * size
                    row_off = j * size
                    
                    win = Window(col_off,row_off, 
                                min(abs(out_profile["width"]-col_off),size), 
                                min(abs((out_profile["height"]-row_off)),size))
                    
                    block_array_img = src.read(window=win)
                    #print(block_array_img.shape)
                    ret1 = np.count_nonzero(block_array_img)>((block_array_img.shape[1]*block_array_img.shape[2])*0.5)
                    if ret1:
                        input_img = np.moveaxis(block_array_img, 0, -1)
                        input_img = input_img[:,:,:3]
                        shp = input_img.shape
                        label_array = label.read(window=win)
                        #print(label_array.shape)
                        #ret2 = np.count_nonzero(label_array)>((label_array.shape[1]*label_array.shape[2])*0.05)
                        ret2 = np.mean(label_array)>255*0.02
                        c+=1
                        label_img = label_array[0,:,:]
                        if ret2:
                            if shp[0]!= size or shp[1]!=size:                   
                                input_img = cv2.copyMakeBorder(input_img,0, size-shp[0], 0, size-shp[1], cv2.BORDER_CONSTANT,value=[1,1,1])
                                label_img = cv2.copyMakeBorder(label_img,0, size-shp[0], 0, size-shp[1], cv2.BORDER_CONSTANT,value=[1,1,1])

                            cv2.imwrite(op_path_imgs+op_name+ "_" + str(c) + ".png", cv2.cvtColor(input_img, cv2.COLOR_RGB2BGR))
                            cv2.imwrite(op_path_lbls+op_name+ "_" + str(c) + ".png", (label_img/255).astype(int))
                        
                        '''
                        else:
                            if shp[0]!= size or shp[1]!=size:                   
                                input_img = cv2.copyMakeBorder(input_img,0, size-shp[0], 0, size-shp[1], cv2.BORDER_CONSTANT,value=[1,1,1])
                                label_img = cv2.copyMakeBorder(label_img,0, size-shp[0], 0, size-shp[1], cv2.BORDER_CONSTANT,value=[1,1,1])

                            cv2.imwrite(no_build_img+op_name+ "_" + str(c) + ".png", cv2.cvtColor(input_img, cv2.COLOR_RGB2BGR))
                            cv2.imwrite(no_build_lbl+op_name+ "_" + str(c) + ".png", (label_img/255).astype(int))
                        '''
                    pbar.update(1)