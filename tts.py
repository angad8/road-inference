import random
import os
import shutil as sh
random.seed(69) 

#data_path = "/home/skylark/AI/OpenCitiesAI/open_cities_ai_tiles/Building/"
#dest_path = "/home/skylark/AI/OpenCitiesAI/OCAI_split/"
data_path = "../../../../../media/skylark/Data/Road_data/Road_Titus_Anot"
dest_path = "/home/skylark/AI/Roads"
if not os.path.exists(dest_path):
    os.mkdir(dest_path)
'''
folders = ["train","test","val"]
for j in folders:
    os.mkdir(os.path.join(dest_path, j))
    os.mkdir(os.path.join(dest_path,j,"Images"))
    os.mkdir(os.path.join(dest_path,j,"Labels"))  
'''
imgs_path = data_path + "/ortho_tiles/"
labels_path = data_path + "/annot_tiles/"
imgs_all = os.listdir(imgs_path)

#codes = ['493701', 'f49f31', '42f235', '33cae6', 'b15fce', 'a017f9', 'abe1a3', 'f883a0', '665946', 'ca041a', '401175', 'd41d81', 'a42435', 'f15272', '353093', '0a4c40', 'bd5c14', '9b8638', '207cc7', 'c7415c', '076995', '3f8360', '75cdfa', '06f252', '425403', '3b20d4', '4e7c7f', 'aee7fd', '825a50', 'e52478', 'bc32f1']
codes = {}
c=0
for fil in imgs_all:
    code = fil.split("_")[2]
    if code not in codes:
        codes[code] = []
        codes[code].append(fil)
        c+=1
    else:
        codes[code].append(fil)
        c+=1
print(c)
print(len(codes))

for clas in codes:
    files = codes[clas]
    # define the size of the sets: ~20% validation, ~10% test, ~70% training (remaining goes to training set)
    validation_count = int(0.2 * len(files))
    test_count = int(0.1 * len(files))
    training_count = len(files) - validation_count - test_count

    # randomly choose ~15% of files to test set
    test_set = random.sample(files, k = test_count)

    # remove already chosen files from original list
    files_wo_test_set = [f for f in files if f not in test_set]

    # randomly chose ~20% of remaining files to validation set
    validation_set = random.sample(files_wo_test_set, k = validation_count)

    # the remaining files going into the training set
    training_set = [f for f in files_wo_test_set if f not in validation_set]

    for i in training_set:
        sh.copy(imgs_path + i, dest_path + "/train/Images/" + i)
        sh.copy(labels_path + i, dest_path  + "/train/Labels/" + i)

    for j in test_set:
        sh.copy(imgs_path + j, dest_path + "/test/Images/" + j)
        sh.copy(labels_path + j, dest_path + "/test/Labels/" + j)

    for k in validation_set:
        sh.copy(imgs_path + k, dest_path + "/val/Images/" + k)
        sh.copy(labels_path + k, dest_path + "/val/Labels/" + k)