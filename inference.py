import os
from types import SimpleNamespace
import torch
import rasterio
from rasterio import features
import geopandas as gpd
from shapely.geometry import shape

from src import BASE_PATH, OUTPUT_PATH
import src.utils as utils
import src.building.utils as building_utils
from src.building.src.predict_tif import main as predict

os.environ["QT_QPA_PLATFORM"] = "offscreen"

# NOTE: Change the model output path in config file in "configs"
# directory


def vectorize_no_processing(raster, pixel_value):
    '''
    Converts the masked raster into shapefile

    Return:
        shape_file: Path to the shape file
        None: if no inference is found
    '''

    raster_tile = rasterio.open(raster)
    raster_data_band_1 = raster_tile.read(1)

    if pixel_value not in raster_data_band_1:
        #  No valid inference found
        return None

    shapes = list(
        features.shapes(raster_data_band_1, transform=raster_tile.transform))

    polygons = [
        shape(geom) for geom, value in shapes
        if (value == pixel_value and shape(geom).area < 10)
    ]

    simple = [
        poly.simplify(0.1) for poly in polygons
    ]

    gdf = gpd.GeoDataFrame(geometry=simple)
    gdf.crs = raster_tile.crs

    filename, _ = utils.get_filename_from_path(raster)
    shape_file = f'{OUTPUT_PATH}{filename}.shp'

    gdf.to_file(driver="ESRI Shapefile", filename=shape_file)
    return shape_file


def load_model(model_path):

    model = torch.load(model_path)

    return model


def infer(processed_raster, model):
    '''
    Does the inference and writes the single channel masked raster.

    0 represent no building, 1 represent building

    Returns:
        output_raster: Path to the masked raster
    '''
    raster_file_name, raster_file_ext = utils.get_filename_from_path(
        processed_raster)
    output_raster = f'{OUTPUT_PATH}{raster_file_name}_output.{raster_file_ext}'

    args = SimpleNamespace(src_path=processed_raster,
                           dst_path=output_raster,
                           batch_size=16,
                           gpu="0",
                           tta=True)
    predict(args, model)
    return output_raster


def inference(context):
    print("Initializing output directories...")
    utils.init_output_dirs()

    print("Downloading raster...")
    raster_path = utils.download_raster(context)

    print("Downloading model...")
    model_path = utils.download_model(context)

    print("Loading model...")
    model = load_model(model_path)

    print("Preprocessing raster...")
    processed_raster = building_utils.preprocess_raster(raster_path)

    raster_crs = building_utils.get_raster_crs(processed_raster)

    print("Inferencing...")
    inference = infer(processed_raster, model)

    print("Vectorizing mask...")
    shape_file = vectorize_no_processing(
        inference,
        pixel_value=1,
    )

    if not shape_file:
        print("No valid inference found...")

        print("Cleanup...")
        utils.cleanup()

        return {"detection": 0}

    print("Cleaning shapefile...")
    #cleaned_shape_file = delete_holes(shape_file)
    geojson, geojson_file = building_utils.shapefile_to_geojson(
        shape_file, context, raster_crs)

    print("Uploading inference to bucket...")
    building_utils.upload_inference(geojson_file, context)

    print("Cleanup...")
    utils.cleanup()

    detection = 0
    if geojson and geojson.get('features'):
        detection = len(geojson.get('features'))

    output = {
        "inference_file_path": context.inference_dest,
        "bucket": os.getenv('CLOUD_BUCKET'),
        "detection": detection
    }

    return output
