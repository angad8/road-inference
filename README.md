# Road-Detection
Detecting all kinds of roads(paved and unpaved) given an orthomosaic

## Requirement 
 `tensorflow>=2.0.0`

## Data preprocessing
[tiling.py](tiling.py) script takes all the tifs (Images and Labels) and tiles it down to 640*640 resolution pngs.
`tts` scripts are for train-test-val split. 


## Prediction
[pred_test.py](pred_test.py) takes the url of any orthomosaic which is available on [spectra](https://spectra-insights.com/dashboard/project) and gives road output in a `.shp file` 

```
$ python3 pred_test.py --url "<download_link>" --ortho_name "<project_name>"
```

## Calculate IOU
[calc_iou.py](calc_iou.py) gives the IOU numbers for the predicted `.shp file` from the model and the ground truth label for a given ortho. \
You can provide three shape files viz. two predicted outputs (before post-processing and after post-processing) and a ground truth label for which you want the metrics.

